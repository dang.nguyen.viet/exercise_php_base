<?php

function veHinhTamGiac($num_of_line)
{
    echo "<div style='position:fixed;left:512px;top:100px'>";
    for ($i = 0; $i < $num_of_line; $i++) {
        for ($j = 0; $j <= $i; $j++) {
            if ($j > 0 and $j < $i and $i < $num_of_line - 1)
                echo "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
            else
                echo "<span>*&nbsp&nbsp&nbsp</span>";
        }
        echo "<br/><br/>";
    }
    echo "<div/>";
}

function veHinhTamGiacRevert($num_of_line)
{
    echo "<div style='position:fixed;left:512px;top:300px'>";
    for ($i = $num_of_line - 1; $i >= 0; $i--) {
        for ($j = 0; $j <= $i; $j++) {
            echo ("*&nbsp&nbsp&nbsp");
        }
        echo "<br/><br/>";
    }
    echo "<div/>";
}

function veHinhVuongRong($width, $height)
{
    echo "<div style='position:fixed;left:256px;top:100px'>";
    $real_height = $height * 2 - 1;
    for ($i = 0; $i < $real_height; $i++) {
        if (($i == 0) or ($i == $real_height - 1)) {
            for ($j = 0; $j < $width - 1; $j++) {
                echo "*&nbsp&nbsp&nbsp";
            }
            echo "*<br/><br/>";
        } else if ($i % 2 == 0) {
            echo "*&nbsp&nbsp&nbsp";
            for ($j = 0; $j < $width - 2; $j++) {
                echo "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
            }
            echo "*<br/><br/>";
        }
    }
    echo "</div>";
}

function veHinhVuong($width, $height)
{
    echo "<div style='position:fixed;left:256px;top:300px'>";
    $real_height = $height * 2 - 1;
    for ($i = 0; $i < $real_height; $i++) {
        if (($i == 0) or ($i == $real_height - 1)) {
            for ($j = 0; $j < $width - 1; $j++) {
                echo "*&nbsp&nbsp&nbsp";
            }
            echo "*<br/><br/>";
        } else if ($i % 2 == 0) {
            echo "*&nbsp&nbsp&nbsp";
            for ($j = 0; $j < $width - 2; $j++) {
                echo "<span>*&nbsp&nbsp&nbsp</span>";
            }
            echo "*<br/><br/>";
        }
    }
    echo "<div/>";
}

function veTamGiacNguoc($width, $height)
{
    echo "<div style='position:fixed;left:64px;top:300px'>";
    for ($i = 0; $i < $height; $i++) {
        for ($j = 0; $j < $width - $i; $j++) {
            if ($j < $i)
                echo "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
            else
                echo "<span>*&nbsp&nbsp&nbsp</span>";
        }
        echo "<br/><br/>";
    }
    echo "<div/>";
}

function veTamGiacRong($width, $height)
{
    echo "<div style='position:fixed;left:64px;top:100px'>";
    for ($i = $height - 1; $i >= 0; $i--) {
        for ($j = 0; $j < $width - $i; $j++) {
            if ($j < $i or ($j > $i and $j < $width - $i - 1) and $i > 0)
                echo "<span style='visibility:hidden' >*&nbsp&nbsp&nbsp</span>";
            else
                echo "<span>*&nbsp&nbsp&nbsp</span>";
        }
        echo "<br/><br/>";
    }
    echo "</div>";
}

veHinhTamGiac(4);
veHinhTamGiacRevert(4);
veHinhVuongRong(8, 4);
veHinhVuong(8, 4);

// điều kiện vẽ tam giác cân: width = height * 2 - 1
veTamGiacNguoc(7, 4);
veTamGiacRong(7, 4);
