<?php
$email = $pass = '';
$email_err = $pass_err = '';
$success = true;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['email'])) {
        $email_err = "Email is required";
    } else {
        $email = $_POST['email'];
        if (!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/", $email)) {
            $email_err = "Invalid email format";
        } else {
            $email_err = '';
        }
    }

    if (empty($_POST['password'])) {
        $pass_err = "Password is required";
    } else {
        $pass = $_POST['password'];
        if (!preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/", $pass)) {
            $pass_err = "Invalid password format";
        }
    }

    if ($email_err != '' or $pass_err != '') $success = false;
    if ($success) echo "<h3 style='color:green'>Register successful</h3>";
}
?>
<style>
    label {
        margin-left: 10px;
    }

    input {
        margin-top: 10px;
        margin-left: 10px;
    }

    span {
        color: red;
    }
</style>
<form method="post" action="form-register.php">
    <label for="email">
        Email<span>*<?= $email_err ?></span>
    </label><br />
    <input type="text" name="email" id="email" placeholder='Email' value='<?= $email ?>' required /><br /><br />
    <label for="password">
        Password<span>*<?= $pass_err ?></span>
    </label><br />
    <input type="password" name="password" id="password" placeholder="Password" required /><br /><br />
    <button type="submit" style='margin-left:10px;'>Register</button>
</form>
